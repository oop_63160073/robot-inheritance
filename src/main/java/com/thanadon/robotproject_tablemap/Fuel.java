/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.robotproject_tablemap;

/**
 *
 * @author Acer
 */
public class Fuel extends Obj {
    private int volumn;

    public Fuel(int x, int y, int volumn) {
        super(x, y, 'F');
        this.volumn = volumn;
    }
    
    public int fill(){
        int vol = volumn;
        volumn = 0;
        symbol = '-';
        return vol;
    }
    
}
