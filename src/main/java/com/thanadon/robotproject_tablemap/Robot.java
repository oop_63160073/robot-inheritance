/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.robotproject_tablemap;

/**
 *
 * @author Acer
 */
public class Robot extends Obj {

    private TableMap map;
    int fuel;

    public Robot(int x, int y, char symbol, TableMap map, int fuel) {
        super(x, y, symbol);
        this.map = map;
        this.fuel = fuel;
    }

    public boolean walk(char direction) {
        switch (direction) {
            case 'N':
            case 'w':
                if (walkNorth()) {
                    return false;
                }
                break;
            case 'S':
            case 's':
                if (walkSouth()) {
                    return false;
                }
                break;
            case 'W':
            case 'a':
                if (walkWest()) {
                    return false;
                }
                break;
            case 'E':
            case 'd':
                if (walkEast()) {
                    return false;
                }
                break;
        }
        foundBomb();
        return true;
    }

    private void foundBomb() {
        if (map.isBomb(x, y)) {
            System.out.println("Founded Bomb!!!!");
        }
    }

    private boolean canWalk(int x, int y) {
        return map.inMap(x, y) && !map.isTree(x, y) && fuel > 0;
    }

    private void reduceFuel() {
        fuel--;
    }

    private boolean walkEast() {
        checkFuel();
        if (canWalk(x + 1, y)) {
            x = x + 1;
            reduceFuel();
        } else {
            return true;
        }
        return false;
    }

    private boolean walkWest() {
        checkFuel();
        if (canWalk(x - 1, y)) {
            x = x - 1;
            reduceFuel();
        } else {
            return true;
        }
        return false;
    }

    private boolean walkSouth() {
        checkFuel();
        if (canWalk(x, y + 1)) {
            y = y + 1;
            reduceFuel();
        } else {
            return true;
        }
        return false;
    }

    private boolean walkNorth() {
        checkFuel();
        if (canWalk(x, y - 1)) {
            y = y - 1;
            reduceFuel();
        } else {
            return true;
        }
        return false;
    }
    
    public void checkFuel(){
        int fuel = map.fillFuel(x, y);
        if(fuel > 0){
            this.fuel += fuel;
        }
    }
    @Override
    public String toString() {
        return "Robot{" + super.toString() + "fuel=" + fuel + '}';
    }

}
